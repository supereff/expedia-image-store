package com.expedia.interview;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.util.List;

public class ImageStoreTest {

	private HotelImageService hotelImageService = new HotelImageService();

	@When("^Insert \"([^\"]*)\" into \"([^\"]*)\"$")
	public void insert_image_into_hotel(String imageId, String hotelName) {
		hotelImageService.addImageToHotel(hotelName, imageId, imageId.getBytes());
		System.gc();//manually call gc to clean up unused images
	}

	@Then("^there should have (\\d+) image\\(s\\)$")
	public void we_should_have_x_images(int size ) {
		Assert.assertEquals("number of images in the store", size, hotelImageService.totalImages());
	}

	@And("contain \"([^\"]*)\"$")
	public void contain(List<String> imagesIds) {
		imagesIds.forEach(imagesId -> Assert.assertTrue(imagesId + "should exist in the store", hotelImageService.containsImage(imagesId)));
	}

}
