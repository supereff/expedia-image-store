Feature: Image

Scenario Outline: Add a Image to a Hotel
  When Insert "imageA" into "Sunshine hotels"
  Then there should have 1 image(s)
  And contain "<images>"
  Examples:
    |images|
    |imageA|


Scenario Outline: Add multiple Images to multiple Hotels
  When Insert "imageA" into "Sunshine hotels"
  When Insert "imageA" into "Stormy hotels"
  When Insert "imageB" into "Beachfront Hotels"
  Then there should have 2 image(s)
  And contain "<images>"
Examples:
  |images|
  |imageA, imageB|


Scenario Outline: Add/Replace Images to Hotels
  When Insert "imageA" into "Sunshine hotels"
  When Insert "imageA" into "Stormy hotels"
  When Insert "imageB" into "Sunshine hotels"
  When Insert "imageB" into "Stormy hotels"
  Then there should have 1 image(s)
  And contain "<images>"
  Examples:
    |images|
    |imageB|

