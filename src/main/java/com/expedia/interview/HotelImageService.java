package com.expedia.interview;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.WeakHashMap;

public class HotelImageService {

	//maps images to hotel to represents a ManyToOne relationship
	private Map<String, String> hotelsImage;
	//maps an image to its id
	private Map<String, byte[]> images;

	public HotelImageService() {
		images = new WeakHashMap<>();
		hotelsImage = new HashMap<>();
	}

	/**
	 * Adds an image to hotel.
	 * Creates an Hotel if it doesnt exist
	 * Creates an Images if it doesnt exist
	 *
	 * @param hotel - name of the hotel
	 * @param imageId - id of image
	 * @param imageContent he content of the image
	 */
	public void addImageToHotel(String hotel, String imageId, byte[] imageContent) {
		Objects.requireNonNull(hotel, "hotel name cannot be null");
		Objects.requireNonNull(imageId, "imageId cannot be null");
		Objects.requireNonNull(imageContent, "image content cannot be null");

		hotelsImage.put(hotel, imageId);
		images.putIfAbsent(imageId, imageContent);
	}

	/**
	 * The size of the store
	 *
	 * @return the actual store size
	 */
	public int totalImages() {
		return images.size();
	}

	public boolean containsImage(String imageId) {
		return images.containsKey(imageId);
	}
}
